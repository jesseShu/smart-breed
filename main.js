import App from './App'

// #ifndef VUE3
import Vue from 'vue'

Vue.config.productionTip = false
App.mpType = 'app'
import store from './store';

// 引入uView
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
import {cuNavigateTo} from '@/utils/index.js'
import {slLoading, slTip} from '@/utils/tip.js'

Vue.filter("moneyFormat", function(value){
	let [int, float] = (value+'').split('.');
	if(!float) {
		return int + '.' + '00';
	}
	if(float.length === 1) {
		return float = `${int}.${float}0`;
	}
	return int+'.'+float;
})


Vue.prototype.$cuNavigateTo = cuNavigateTo
Vue.prototype.$cuLoading = slLoading
Vue.prototype.$cuTip = slTip

const app = new Vue({
	store,
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif